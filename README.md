# ZKS_2021_semestral_work_forgaada

Projekt pro testování aplikace StrengthLevel pomocí knihovny Cypress.

Odkaz na webovou aplikaci : https://strengthlevel.com 

## Návod pro spuštění

### Požadavky

- Node (v14.* LTS)
- nainstalovaný npm (Node Package Manager, aktuálně v7)

### Instalace

- naklonovat si repozitář `git@gitlab.fel.cvut.cz:forgaada/zks_2021_semestral_work_forgaada.git`
- v repozitáři otevřít terminál a spustit následující příkazy:
    - `npm install`
    - `npm run start` (pro spuštění Cypress testů s GUI)
    - `npm run test` (spuštění bez GUI)