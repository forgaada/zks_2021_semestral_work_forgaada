class LoginPage {
    get loginInput(): Cypress.Chainable {
        return cy.get('[name="email"]');
    }

    get passwordInput(): Cypress.Chainable {
        return cy.get('[name="password"]');
    }

    get loginButton(): Cypress.Chainable {
        return cy.get('[type="submit"]');
    }
}

export default new LoginPage();