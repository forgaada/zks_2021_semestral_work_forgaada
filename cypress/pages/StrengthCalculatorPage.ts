class StrengthCalculatorPage {
    get genderSelect(): Cypress.Chainable {
        return cy.get('[name="gender"]');
    }

    get ageSelect(): Cypress.Chainable {
        return cy.get('[name="age"]');
    }

    get bodyMassInput(): Cypress.Chainable {
        return cy.get('[name="bodymass"]');
    }

    get bodyMassUnitSelect(): Cypress.Chainable {
        return cy.get('[name="bodymassunit"]');
    }

    get exerciseSelect(): Cypress.Chainable {
        return cy.get('.control.control--modal.has-icons-left.has-icons-right');
    }

    get liftMassInput(): Cypress.Chainable {
        return cy.get('[name="liftmass"]');
    }

    get liftMassUnitSelect(): Cypress.Chainable {
        return cy.get('[name="liftmassunit"]');
    }

    get repetitionsInput(): Cypress.Chainable {
        return cy.get('[name="repetitions"]');
    }

    get calculateButton(): Cypress.Chainable {
        return cy.get('[value="Calculate Strength"]');
    }
}

export default new StrengthCalculatorPage();