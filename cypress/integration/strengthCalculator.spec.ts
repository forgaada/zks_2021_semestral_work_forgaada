import strengthCalculator from "../pages/StrengthCalculatorPage";

context('Test strength calculation form on home page', () => {
    beforeEach(() => {
        cy.login(Cypress.env('username'), Cypress.env('password'));
        cy.fixture("records.json").as("records");
    })

    it('Should successfully calculate strength for lift records', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}`;

        cy.fixture("records").then((records) => {
            for (let i = 0; i < records.length; i++) {
                cy.visit('');
                strengthCalculator.genderSelect.select(records[i].gender);
                strengthCalculator.ageSelect.select(records[i].age.toString());
                strengthCalculator.bodyMassInput.clear().type(records[i].bodyWeight);
                strengthCalculator.bodyMassUnitSelect.select(records[i].bodyWeightUnit);
                strengthCalculator.exerciseSelect.click();
                cy.get('span').contains(records[i].exercise).click({force: true});
                strengthCalculator.liftMassInput.clear().type(records[i].lift);
                strengthCalculator.liftMassUnitSelect.select(records[i].liftUnit);
                strengthCalculator.repetitionsInput.clear().type(records[i].repetitions);
                strengthCalculator.calculateButton.click();

                if (records[i].status == 'valid') {
                    // assert that page was loaded properly
                    cy.url().should('eq', correctFinalUrl);
                    // assert that strength level has been calculated
                    cy.contains(`Your Strength Level for ${records[i].exercise}`).should('exist');
                } else {
                    // assert that page was loaded properly
                    cy.url().should('eq', correctFinalUrl);
                    // assert that strength level hasn't been calculated
                    cy.contains(`Your Strength Level for ${records[i].exercise}`).should('not.exist');
                }
            }
        });
    });

});