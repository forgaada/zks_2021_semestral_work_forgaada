context('Test managing information about workouts workouts section', () => {
    beforeEach(() => {
        cy.login(Cypress.env('username'), Cypress.env('password'));
        cy.fixture("exercises.json").as("exercises");
    })

    it('Should successfully add new workout', () => {
        const today = new Date();
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/${today.getFullYear()}/`;

        cy.addWorkout();

        // assert that user has been redirected
        cy.url().should('include', correctFinalUrl);
    });

    it('Should successfully delete created workout', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/workouts`;

        cy.addWorkout();
        cy.contains('Delete Workout').first().click();

        // assert that user has been redirected
        cy.url().should('eq', correctFinalUrl);
    });

    it('Add exercises to created workout', () => {
        const today = new Date();
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/${today.getFullYear()}/`;

        cy.fixture("exercises").then((exercises) => {
            for (let i = 0; i < exercises.length; i++) {
                cy.addWorkout();
                cy.contains('Add Exercise').first().click();
                cy.contains(exercises[i].name).click({force: true});

                // assert that user has been redirected
                cy.url().should('include', correctFinalUrl);
                // assert that exercise has been created
                cy.contains(exercises[i].name).should('exist');

                // delete workout session
                cy.get('span').contains('Delete Workout').first().click();
            }
        });
    });

});