context('Test managing information about user body weight stats', () => {
    beforeEach(() => {
        cy.login(Cypress.env('username'), Cypress.env('password'));
        cy.fixture("bodymasses.json").as("bodymasses");
    });

    it('Add body weight goal successfully', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/bodyweight`;
        const goalBodyMass = "80";

        cy.visit(`111709-cypresstestuser/bodyweight`);
        cy.contains('Add Goal').click();
        cy.get('[id="goal_bodymass"]').clear().type(goalBodyMass);
        cy.get('[value="addbodyweightgoal"]').click();

        // check correct url
        cy.url().should('eq', correctFinalUrl);
        // check body weight goal added
        cy.get('span').contains('Bodyweight goal added').should('exist');
    });

    it('Add body mass record to body weights', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/bodyweight`;

        cy.fixture("bodymasses").then((bodymasses) => {
            for (let i = 0; i < bodymasses.length; i++) {
                cy.addBodyWeight(bodymasses[i].weight, bodymasses[i].bodyFat, bodymasses[i].value ? bodymasses[i].value : undefined);

                if (bodymasses[i].status == 'valid') {
                    // check page url
                    cy.url().should('eq', correctFinalUrl);
                    // assert that exercise has been created
                    cy.contains(bodymasses[i].weight + " kg").should('exist');
                    // check body weight record added
                    cy.get('span').contains('Bodyweight added').should('exist');
                } else {
                    // check page url
                    cy.url().should('eq', correctFinalUrl);
                    // check body weight record has been not added
                    cy.get('span').contains('Bodyweight added').should('not.exist');
                }
            }
        });
    });

    it('Delete last body mass record successfully', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser/bodyweight`;

        // add body weight record
        cy.addBodyWeight("85", "scales", "10");

        // delete last body weight record
        cy.get('span').contains('Delete').first().click({force: true});

        // check correct url
        cy.url().should('eq', correctFinalUrl);
    });

});