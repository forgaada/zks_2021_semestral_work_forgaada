import login from "../pages/LoginPage";

context('Test login to application', () => {

  it('Should successfully login user', () => {
    const correctFinalUrl = `${Cypress.config().baseUrl}111709-cypresstestuser`;

    cy.visit('login');
    login.loginInput.type(Cypress.env('username'));
    login.passwordInput.type(Cypress.env('password'));
    login.loginButton.click();

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);
  });

  it('Should fail to login user', () => {
    const correctFinalUrl = `${Cypress.config().baseUrl}login`;

    cy.visit('login');
    login.loginInput.type('test@gmail.com');
    login.passwordInput.type('test1234');
    login.loginButton.click();

    // assert that page was loaded properly
    cy.url().should('eq', correctFinalUrl);
    // assert error message has been thrown
    cy.contains('Could not find your account, please check your email and password').should('exist');
  });

});
