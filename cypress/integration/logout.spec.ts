context('Test logout from application', () => {
    beforeEach(() => {
        cy.login(Cypress.env('username'), Cypress.env('password'));
        cy.visit('/')
    })

    it('Should successfully logout user', () => {
        const correctFinalUrl = `${Cypress.config().baseUrl}`;

        cy.visit('111709-cypresstestuser');
        cy.get('span').contains('Sign Out').click({force: true});

        // assert that user has been redirected after logout
        cy.url().should('eq', correctFinalUrl);
    });

});