declare namespace Cypress {
    interface Chainable {
        login(username: string, password: string): void;
        addWorkout(): void;
        addBodyWeight(weight: string, bodyFat: string, value : any): void;
    }
}

const login = (username : string, password: string) => {
    cy.visit('login')
        .get('[name="email"]')
        .type(username)
        .get('[name="password"]')
        .type(password)
        .get('[type="submit"]')
        .click();
};

const addWorkout = () => {
    cy.visit(`111709-cypresstestuser/workouts`);
    cy.contains('Add Workout').click();
    cy.get('[type="submit"]').contains('Today').click();
};

const addBodyWeight = (weight: string, bodyFat: string, value : any) => {
    cy.visit(`111709-cypresstestuser/bodyweight`);
    cy.contains('Add Bodyweight').click();
    cy.get('[name="bodymass"]').clear().type(weight);
    cy.get('[name="bodyfattechnique"]').select(bodyFat);
    if (value !== undefined) {
        cy.get('[name="bodyfatpercentage"]').clear().type(value);
    }
    cy.get('[value="addbodyweight"]').click();
};

Cypress.Commands.add('login', login);
Cypress.Commands.add('addWorkout', addWorkout);
Cypress.Commands.add('addBodyWeight', addBodyWeight);